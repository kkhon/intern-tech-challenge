package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/coreos/go-semver/semver"
	"github.com/google/go-github/github"
)

// Versions is type in semver lib
type Versions []*semver.Version

func (s Versions) Len() int {
	return len(s)
}

func (s Versions) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s Versions) Less(i, j int) bool {
	return s[i].LessThan(*s[j])
}

// ReverseSort sorts the given slice of Versions from highest to lowest
func ReverseSort(versions []*semver.Version) {
	sort.Sort(sort.Reverse(Versions(versions)))
}

// ProcessFile will retruns numbers of repo, repo owner, repo name and minimum version
func ProcessFile(path string) (int, []string, []string, []string) {

	fileHandle, err := os.Open(path) // open file

	if err != nil {
		panic(err)
	}

	defer fileHandle.Close() // close file when finished

	fileScanner := bufio.NewScanner(fileHandle)

	owner := make([]string, 0)
	repo := make([]string, 0)
	minv := make([]string, 0)

	// Split the sentence to words
	i := 0
	for fileScanner.Scan() {
		// Scan for each line in text file & skip first line
		if i >= 1 {
			temp := fileScanner.Text()
			if strings.Contains(temp, "/") && strings.Contains(temp, ",") {
				s0 := strings.Split(temp, ",")
				s1 := strings.Split(s0[0], "/")
				owner = append(owner, s1[0])
				repo = append(repo, s1[1])
				minv = append(minv, s0[1])
			} else {
				fmt.Println("Format in text file is not correct!")
				i-- // decrease the counter for invalid format
			}
		}
		i++
	}
	return i - 1, owner, repo, minv
}

// LatestVersions returns a sorted slice with the highest version as its first element and the highest version of the smaller minor versions in a descending order
func LatestVersions(releases []*semver.Version, minVersion *semver.Version) []*semver.Version {

	var versionSlice []*semver.Version
	semver.Sort(releases) // sort the slice
	for _, releaseVersion := range releases {
		result := releaseVersion.Compare(*minVersion)
		// Assume we only select stable release versions
		if result == 1 && releaseVersion.PreRelease == "" && releaseVersion.Metadata == "" {
			s := releaseVersion.String()
			versionSlice = append(versionSlice, semver.New(s))
		}
	}

	// Loop over the slice and remove lowest patch versions of same minor
	for i := 0; i < len(versionSlice)-1; i++ {
		if versionSlice[i].Major == versionSlice[i+1].Major && versionSlice[i].Minor == versionSlice[i+1].Minor && versionSlice[i].Patch <= versionSlice[i+1].Patch {
			versionSlice = append(versionSlice[:i], versionSlice[i+1:]...)
			i-- // form the remove item index to start iterate next item
		}
	}

	ReverseSort(versionSlice) //sort the slice from highest to lowest
	return versionSlice
}

// Here we implement the basics of communicating with github through the library as well as printing the version
// You will need to implement LatestVersions function as well as make this application support the file format outlined in the README
// Please use the format defined by the fmt.Printf line at the bottom, as we will define a passing coding challenge as one that outputs
// the correct information, including this line
func main() {

	args := os.Args
	if len(args) != 2 {
		fmt.Println("Usage: ./main FilePath")
		os.Exit(1)
	}

	size, owner, repo, minv := ProcessFile(args[1])

	for i := 0; i < size; i++ {
		// Github
		client := github.NewClient(nil)
		ctx := context.Background()
		opt := &github.ListOptions{PerPage: 10}
		releases, _, err := client.Repositories.ListReleases(ctx, owner[i], repo[i], opt)
		if err != nil {
			fmt.Println(err) // panic is fatal for program, this can keep the program running
			continue
		}
		minVersion := semver.New(minv[i])
		allReleases := make([]*semver.Version, len(releases))
		for i, release := range releases {
			versionString := *release.TagName
			if versionString[0] == 'v' {
				versionString = versionString[1:]
			}
			allReleases[i] = semver.New(versionString)
		}
		versionSlice := LatestVersions(allReleases, minVersion)
		fmt.Printf("latest versions of %s/%s: %s\n", owner[i], repo[i], versionSlice)
	}
}
